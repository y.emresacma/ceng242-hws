module Hw1 where

type Mapping = [(String, String, String)]
data AST = EmptyAST | ASTNode String AST AST deriving (Show, Read)
    
    
writeExpression :: (AST, Mapping) -> String
evaluateAST :: (AST, Mapping) -> (AST, String)
-- DO NOT MODIFY OR DELETE THE LINES ABOVE -- 
-- IMPLEMENT writeExpression and evaluateAST FUNCTION ACCORDING TO GIVEN SIGNATURES -- 
    
extractFirst [] = []
extractFirst (a:l) = l
extractFirst2 [] = []
extractFirst2 (a:b:l) = l

traverses EmptyAST = []
traverses (ASTNode a left right) = (traverses left) ++ (traverses right) ++ [a]

getRidOf [] = []
getRidOf (a:rest) | rest == [] = [a] 
                  | (rest!!0) == "num" = ([a] ++ (getRidOf (extractFirst rest)))
                  | (rest!!0) == "str" = (["\"" ++ a ++ "\""] ++ (getRidOf (extractFirst rest)))
                  | otherwise = ([a] ++(getRidOf rest))
getRidOf2 [] = []
getRidOf2 (a:rest) | rest == [] = [a] 
                | (rest!!0) == "num" = ([a] ++ (getRidOf2 (extractFirst rest)))
                | (rest!!0) == "str" = ([a] ++ (getRidOf2 (extractFirst rest)))
                | otherwise = ([a] ++(getRidOf2 rest))
    
writeHelper [] = []
writeHelper (a:rest) | rest == [] = [a] 
                     | (rest!!0) == "negate" = writeHelper ([ "(-" ++ a ++ ")"] ++ (extractFirst rest))
                     | (rest!!0) == "len" = writeHelper (["(length " ++ a ++ ")"] ++ (extractFirst rest))
                     | (rest!!0) == "plus" = [a] ++ rest
                     | (rest!!0) == "cat" = [a] ++ rest
                     | (rest!!0) == "times" = [a] ++ rest
                     | (rest!!1) == "plus" = writeHelper (["(" ++ a ++ "+" ++ rest!!0 ++ ")"] ++  (extractFirst2 rest))
                     | (rest!!1) == "times" = writeHelper (["(" ++ a ++ "*" ++ rest!!0 ++ ")"] ++ (extractFirst2 rest))   
                     | (rest!!1) == "cat" = writeHelper (["(" ++ a ++ "++" ++ rest!!0 ++ ")"] ++ (extractFirst2 rest))
                     | (rest!!1) == "negate"= writeHelper ([a] ++ ["(-" ++ rest!!0 ++ ")"] ++ (extractFirst2 rest))
                     | (rest!!1) == "len"= writeHelper ([a] ++ ["length(" ++ rest!!0 ++ ")"] ++  (extractFirst2 rest))
                     | otherwise = writeHelper ([a] ++ (writeHelper rest))
        
get1 (a, b, c) = a 
get2 (a, b, c) = b
get3 (a, b, c) = c     

writeHelper2 (a:l) str | (l == [] && (get2 a) == "str") = "let " ++ str ++ (get1 a) ++ "=" ++ "\"" ++ (get3 a) ++ "\"" ++ " in "
                       | (l == [] && (get2 a) == "num") = "let " ++ str ++ (get1 a) ++ "=" ++ (get3 a) ++ " in "
                       | ( (get2 a) == "str") = (writeHelper2 l (str ++ (get1 a) ++ "=" ++ "\"" ++ (get3 a) ++ "\";"))
                       | ( (get2 a) == "num") = (writeHelper2 l (str ++ (get1 a) ++ "=" ++ (get3 a) ++ ";")) 
                       | otherwise = str
    
convert2String [] = ""
convert2String [a] = a

writeExpression (EmptyAST, []) = ""
writeExpression ((ASTNode a left right), []) = (convert2String ((writeHelper (getRidOf (traverses (ASTNode a left right))))))
writeExpression ((ASTNode a left right), l) = ((writeHelper2 l "") ++ (convert2String (writeHelper (getRidOf (traverses (ASTNode a left right))))))
    
evaulateHelper1 [] = []
evaulateHelper1 (a:rest) | rest == [] = [a]
                         | (rest!!0) == "len" = evaulateHelper1 ([show (length a)] ++ (extractFirst rest))
                         | (rest!!0) == "negate" = evaulateHelper1 ([show (-(read a::Int))] ++ (extractFirst rest))
                         | (rest!!0) == "plus" = [a] ++ rest
                         | (rest!!0) == "times" = [a] ++ rest
                         | (rest!!0) == "cat" = [a] ++ rest
                         | (rest!!1) == "len" = evaulateHelper1 ([a] ++ [show (length (rest!!0))] ++ (extractFirst2 rest))
                         | (rest!!1) == "negate" = evaulateHelper1 ([a] ++ [show (-(read (rest!!0)::Int))] ++ (extractFirst2 (rest)))
                         | (rest!!1) == "plus" = evaulateHelper1 ([show ((read a::Int) + (read (rest!!0)::Int))] ++ (extractFirst2 rest))
                         | (rest!!1) == "times" = evaulateHelper1 ([show ((read a::Int) * (read (rest!!0)::Int))] ++ (extractFirst2 rest))
                         | (rest!!1) == "cat" = evaulateHelper1 ([a ++ rest!!0] ++ (extractFirst2 rest))
                         | otherwise = evaulateHelper1 ([a] ++ (evaulateHelper1 rest))

correctTree (EmptyAST) [] = EmptyAST
correctTree (EmptyAST) (a:l) = EmptyAST
correctTree (ASTNode a left right) [] = (ASTNode a left right)
correctTree (ASTNode a left right) (x:l) | (get1 x) == a = (ASTNode (get2 x) (ASTNode (get3 x) (correctTree left l) (correctTree right l)) EmptyAST)
                                         | otherwise = correctTree (ASTNode a (correctTree left [x]) (correctTree right [x])) l

evaluateAST (EmptyAST, []) = (EmptyAST, "")
evaluateAST ((ASTNode a left right), []) = ((ASTNode a left right) ,convert2String (evaulateHelper1 (getRidOf2 (traverses ((ASTNode a left right))))))
evaluateAST ((ASTNode a left right), lst) = ((correctTree (ASTNode a left right) lst), convert2String (evaulateHelper1 (getRidOf2 (traverses ((correctTree (ASTNode a left right) lst))))))