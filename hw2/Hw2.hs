module Hw2 where

  data ASTResult = ASTError String | ASTJust (String, String, Int) deriving (Show, Read)
  data ASTDatum = ASTSimpleDatum String | ASTLetDatum String deriving (Show, Read)
  data AST = EmptyAST | ASTNode ASTDatum AST AST deriving (Show, Read)
  
  isNumber :: String -> Bool


  eagerEvaluation :: AST -> ASTResult
  normalEvaluation :: AST -> ASTResult
  -- DO NOT MODIFY OR DELETE THE LINES ABOVE -- 
  -- IMPLEMENT isNumber, eagerEvaluation and normalEvaluation FUNCTIONS ACCORDING TO GIVEN SIGNATURES -- 


  f = tail . foldr (\x ~(r:rs) -> (x : r) : r : map (x :) rs) [[]]
  extractRest a =  (f a)!!0

  isNumberHelper "" = True
  isNumberHelper a =
    if ((take 1 a) == "0" || (take 1 a) == "1" || (take 1 a) == "2" || (take 1 a) == "3" || (take 1 a) == "4" || (take 1 a) == "5" || (take 1 a) == "6" || (take 1 a) == "7" || (take 1 a) == "8" || (take 1 a) == "9")
      then isNumberHelper (extractRest a)
    else False

  isNumber a = 
    if a == "" 
      then False
    else if a == "-"
      then False
    else if (take 1 a) == "-"
      then isNumberHelper (extractRest a)
    else isNumberHelper a

  extractFirst [] = []
  extractFirst (a:l) = l
  extractFirst2 [] = []
  extractFirst2 (a:b:l) = l

  isIn x [] = False
  isIn x (a:l) = if (get1 a) == x then True else (isIn x l)

  get1 (a, b) = a 
  get2 (a, b) = b
  
  get31 (a, b, c) = a
  get32 (a, b, c) = b
  get33 (a, b, c) = c

  get4_1 (a, b, c, d) = a
  get4_2 (a, b, c, d) = b
  get4_3 (a, b, c, d) = c
  get4_4 (a, b, c, d) = d
    
  convert2String [] = ""
  convert2String [a] = a
  convert2List a = [a]

  changeVariable [] (x, y) stack = ([], stack)
  changeVariable (a:l) (x, y) stack | a == y = changeVariable l (x, y) (stack ++ [x])
                                    | a /= y = changeVariable l (x, y) (stack ++ [a])
  
  getVar (ASTNode (ASTSimpleDatum a) left right) = a

  isNum (ASTNode (ASTSimpleDatum a) EmptyAST EmptyAST) = (isNumber a)
  isNum (ASTNode (ASTSimpleDatum a) left right) = if (a == "num") then True else False

  isOp str =
    if str == "len" || str == "negate" || str == "plus" || str == "times" || str == "cat" 
      then True
    else 
      False

  isNumOp a =
    if (a == "plus" || a == "times") 
      then True
    else False
  
  isStrOp a =
    if (a == "cat")
      then True
    else False 
  

  change var (a:l) | var == (get1 a) = (get2 a)
                   | otherwise = change var l
  
  add [][] = []
  add [] l = l
  add l [] = l
  add (a:b) (c:d) | (get1 a) == (get1 c) = ([a] ++ d)
                  | otherwise = [c] ++ (add [a] d)
  
  eliminateTree EmptyAST = EmptyAST
  eliminateTree (ASTNode (ASTSimpleDatum var) left right) =
    if (var == "num" || var == "str")
      then (ASTNode (ASTSimpleDatum (getVar left)) EmptyAST EmptyAST)
    else (ASTNode (ASTSimpleDatum var) (eliminateTree left) (eliminateTree right))
  eliminateTree (ASTNode (ASTLetDatum var) left right) = (ASTNode (ASTLetDatum var) (eliminateTree left) (eliminateTree right))
            
  calculate EmptyAST msg str tuple = (msg, str, tuple)
  calculate (ASTNode (ASTSimpleDatum var) left right) msg str tuple | msg /= "" = (msg, "", [])
                                                                    | var == "num" = if (isNum left) then ("", "num", tuple) else ("the value '" ++ (getVar left) ++ "' is not a number!", "", tuple)
                                                                    | var == "str" = ("", "str", tuple)
                                                                    | var == "negate" = 
                                                                      if (get31 (calculate left "" "" tuple)) /= "" 
                                                                        then ((get31 (calculate left "" "" tuple)), "", tuple)
                                                                      else if (get32 (calculate left "" "" tuple)) /= "num"
                                                                        then ("negate operation is not defined on str!", "", tuple)
                                                                      else ("", "num", tuple)
                                                                    | var == "len" = 
                                                                      if (get31 (calculate left "" "" tuple)) /= ""
                                                                        then ((get31 (calculate left "" "" tuple)), "", tuple)
                                                                      else if (get32 (calculate left "" "" tuple)) /= "str"
                                                                        then ("len operation is not defined on num!", "", tuple)
                                                                      else ("", "num", tuple)
                                                                    | (isNumOp var) = 
                                                                      if (get31 (calculate left "" "" tuple)) /= ""
                                                                        then ((get31 (calculate left "" "" tuple)), "", tuple)
                                                                      else if (get31 (calculate right "" "" tuple)) /= ""
                                                                        then ((get31 (calculate right "" "" tuple)), "", tuple)
                                                                      else if ((get32 (calculate left "" "" tuple)) == "num" && (get32 (calculate right "" "" tuple)) == "num")
                                                                        then ("","num", tuple)
                                                                      else ((var ++ " operation is not defined between " ++ (get32 (calculate left "" "" tuple)) ++ " and " ++ (get32 (calculate right "" "" tuple)) ++ "!"), "", tuple)
                                                                    | (isStrOp var) =
                                                                      if (get31 (calculate left "" "" tuple)) /= ""
                                                                        then ((get31 (calculate left "" "" tuple)), "", tuple)
                                                                      else if (get31 (calculate right "" "" tuple)) /= ""
                                                                        then ((get31 (calculate right "" "" tuple)), "", tuple)
                                                                      else if ((get32 (calculate left "" "" tuple)) == "str" && (get32 (calculate right "" "" tuple)) == "str")
                                                                        then ("","str", tuple)
                                                                      else ((var ++ " operation is not defined between " ++ (get32 (calculate left "" "" tuple)) ++ " and " ++ (get32 (calculate right "" "" tuple)) ++ "!"), "", tuple)
                                                                    | (isIn var tuple) = ("", (change var tuple), tuple)
                                                                    | otherwise = (msg,str,tuple)
  calculate (ASTNode (ASTLetDatum var) left right) msg str tuple | msg /= "" = (msg, "", [])
                                                                 | (get31 (calculate left "" "" tuple)) /= "" = ((get31 (calculate left "" "" tuple)), "", [])
                                                                 | otherwise = calculate right "" "" (add [(var, (get32 (calculate left "" "" tuple)) )] tuple)
  

  evaluate (ASTNode (ASTSimpleDatum var) left right) tuple | var == "plus" = ((ASTNode (ASTSimpleDatum (show ((read (getVar (get1 (evaluate left tuple)))::Int) + (read (getVar (get1 (evaluate right tuple)))::Int)))) EmptyAST EmptyAST), tuple)
                                                           | var == "times" = ((ASTNode (ASTSimpleDatum (show ((read (getVar (get1 (evaluate left tuple)))::Int) * ((read (getVar (get1 (evaluate right tuple)))::Int))))) EmptyAST EmptyAST), tuple)
                                                           | var == "negate" = ((ASTNode (ASTSimpleDatum (show (-(read (getVar (get1 (evaluate left tuple)))::Int)))) EmptyAST EmptyAST), tuple)
                                                           | var == "len" = ((ASTNode (ASTSimpleDatum (show (length (getVar (get1 (evaluate left tuple)))))) EmptyAST EmptyAST), tuple)
                                                           | var == "cat" = ((ASTNode (ASTSimpleDatum ((getVar (get1 (evaluate left tuple))) ++ (getVar (get1 (evaluate right tuple))) )) EmptyAST EmptyAST), tuple)
                                                           | (isIn var tuple) = evaluate (ASTNode (ASTSimpleDatum (change var tuple)) left right) tuple
                                                           | otherwise = ((ASTNode (ASTSimpleDatum var) EmptyAST EmptyAST), tuple)
  evaluate (ASTNode (ASTLetDatum var) left right) tuple = evaluate right (add [(var, (getVar (get1 (evaluate left tuple))))] tuple)
                                                                        

  

  countEager EmptyAST = 0
  countEager (ASTNode (ASTSimpleDatum a) left right) = 
    if (isOp a)
      then (1 + (countEager left) + (countEager right))
    else ((countEager left) + (countEager right))
  countEager (ASTNode (ASTLetDatum a) left right) = ((countEager left) + (countEager right))

  eagerEvaluation EmptyAST = (ASTError "Nothing")
  eagerEvaluation tree = 
    if (get31 (calculate tree "" "" [])) /= ""
      then ASTError (get31 (calculate tree "" "" []))
    else ASTJust ((getVar (get1 (evaluate (eliminateTree tree) []))), (get32 (calculate tree "" "" [])), (countEager tree))

  find x (a:l) = 
    if (get1 a) == x
      then (get2 a)
    else find x l

  countNormal EmptyAST _ = 0
  countNormal (ASTNode (ASTSimpleDatum a) left right) tuple = 
    if (isOp a)
      then (1 + (countNormal left tuple) + (countNormal right tuple))
    else if (isIn a tuple)
      then ((find a tuple) + (countNormal left tuple) + (countNormal right tuple))
    else ((countNormal left tuple) + (countNormal right tuple))
  countNormal (ASTNode (ASTLetDatum a) left right) tuple = countNormal right (add [(a, (countNormal left tuple))] tuple)
  
  normalEvaluation EmptyAST = (ASTError "Nothing")
  normalEvaluation tree = 
    if (get31 (calculate tree "" "" [])) /= ""
      then ASTError (get31 (calculate tree "" "" []))
    else ASTJust ((getVar (get1 (evaluate (eliminateTree tree) []))), (get32 (calculate tree "" "" [])), (countNormal tree []))

  