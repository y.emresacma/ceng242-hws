#include "Race.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE 
*/

Race::Race(std::string race_name): 
    race_name(race_name), 
    average_laptime(Utilizer::generateAverageLaptime()),
    head(nullptr) {
}



Race::Race(const Race& rhs): 
    race_name(rhs.getRaceName()),
    average_laptime(rhs.getAverageLaptime()) {

    if (rhs.getHead() == nullptr) {
        this->head = nullptr;
    }

    else {
        this->head = new Car(rhs.getHead()->getDriverName());
        Car *current = this->head;
        Car *rhsHead = rhs.getHead();
        Car *currentRhs = rhsHead;
        current->setPerformance(currentRhs->getPerformance());

        while (currentRhs->getNext() != nullptr) {
            current->setNext(new Car(currentRhs->getNext()->getDriverName()));
            current = current->getNext();
            currentRhs = currentRhs->getNext();
            current->setPerformance(currentRhs->getPerformance());
        }
    }

}

Race::~Race() {
    /*Car *currentCar, *tempCar;
    Laptime *currentLap, *tempLap;

    currentCar = this->head;
    while (currentCar != nullptr) {
        currentLap = currentCar->getHead();

        while (currentLap != nullptr) {
            tempLap = currentLap->getNext();
            delete currentLap;
            currentLap = tempLap;
        }
        currentCar->setHead(nullptr);

        tempCar = currentCar->getNext();
        delete currentCar;
        currentCar = tempCar;
    }
    this->head = nullptr;
    */
}

std::string Race::getRaceName() const {
    return this->race_name;
}

std::string & Race::getRaceName() {
    return this->race_name;
}

void Race::setRaceName(std::string name) {
    this->race_name = name;
}

Laptime & Race::getAverageLaptime() {
    return this->average_laptime;
}

Laptime Race::getAverageLaptime() const {
    return this->average_laptime;
}

void Race::setAverageLaptime(Laptime laptime) {
    this->average_laptime = laptime;
}

Car* & Race::getHead() {
    return this->head;
}

Car* Race::getHead() const {
    return this->head;
}

void Race::setHead(Car* head) {
    this->head = head;
}

std::string Race::randomString() {
    std::string str = "AAAAA";
    
    // string sequence
    str[0] = rand() % 26 + 65;
    str[1] = rand() % 26 + 65;
    str[2] = rand() % 26 + 65;
    
    // number sequence
    str[3] = rand() % 10 + 48;
    str[4] = rand() % 10 + 48;
    str[5] = rand() % 10 + 48;

    return str;
}

void Race::addCartoRace() {
    Car* currentCar = this->head;

    if (currentCar == nullptr) {
        this->head = new Car(randomString());
        return;
    }

    while (currentCar->getNext() != nullptr) {
        currentCar = currentCar->getNext();
    }

    currentCar->setNext(new Car(randomString()));
}

void Race::addCartoRace(Car& car) {
    Car* currentCar = this->head;

    if (currentCar == nullptr) {
        this->head = &car;
        return;
    }

    while (currentCar->getNext() != nullptr) {
        currentCar = currentCar->getNext();
    }

    currentCar->setNext(&car);
}

int Race::getNumberOfCarsinRace() {
    int count = 0;
    Car *currentCar = this->head;

    while (currentCar != nullptr) {
        currentCar = currentCar->getNext();
        count++;
    }

    return count;
}

void Race::goBacktoLap(int lap) {
    Car *currentCar = this->head;
    Laptime *currentLap, *prevLap = nullptr, *temp;
    int count;

    while (currentCar != nullptr) {
        currentLap = currentCar->getHead();
        count = -1;
        
        while (currentLap != nullptr && count++ != lap) {
            prevLap = currentLap;
            currentLap = currentLap->getNext();
        }

        if (prevLap != nullptr) {
            prevLap->setNext(nullptr);
        }

        while (currentLap != nullptr) {
            temp = currentLap->getNext();
            currentLap->setNext(nullptr);
            delete currentLap;
            currentLap = temp;
        }

        currentCar = currentCar->getNext();
    }

    this->sortCars(this->head);
}

void Race::operator++() {
    Car *currentCar = this->head;

    while (currentCar != nullptr) {
        currentCar->Lap(this->getAverageLaptime());
        currentCar = currentCar->getNext();
    }

    this->sortCars(this->head);
}

void Race::operator--() {
    Car *currentCar = this->head;
    Laptime *currentLap, *prevLap = nullptr;


    while (currentCar != nullptr) {
        currentLap = currentCar->getHead();

        while (currentLap != nullptr && currentLap->getNext() != nullptr) {
            prevLap = currentLap;
            currentLap = currentLap->getNext(); 
        }

        if (currentLap != nullptr) {
            if (prevLap == nullptr) {
                currentCar->setHead(nullptr);
                delete currentLap;
                currentLap = nullptr;
            }

            else {
                prevLap->setNext(nullptr);
                delete currentLap;
                currentLap = nullptr;
            }
        }

        currentCar = currentCar->getNext();
    }

    this->sortCars(this->head);
}

Car Race::operator[](const int car_in_position) {
    int count = 0;
    Car *currentCar = this->head;

    while (count++ != car_in_position) {
        currentCar = currentCar->getNext();
    }

    return *currentCar;
}

Car Race::operator[](std::string driver_name) {
    Car *currentCar = this->head;

    while (currentCar->getDriverName() != driver_name) {
        currentCar = currentCar->getNext();
    }

    return *currentCar;
}

void Race::clear() {
    Car *currentCar = this->head, *tempCar = nullptr;
    Laptime *currentLap, *tempLap = nullptr;

    while (currentCar != nullptr) {
        currentLap = currentCar->getHead();

        while (currentLap != nullptr) {
            tempLap = currentLap->getNext();
            currentLap->setNext(nullptr);
            
            delete currentLap;
            currentLap = tempLap;
        }

        currentCar->setHead(nullptr);
        tempCar = currentCar->getNext();
        currentCar->setNext(nullptr);

        delete currentCar;
        currentCar = tempCar;
    }

    this->head = nullptr;
}

Race& Race::operator=(const Race& rhs) {
    this->race_name = rhs.getRaceName();
    this->average_laptime = rhs.getAverageLaptime();
    this->head = rhs.getHead();
    return *this;
}

void Race::sortCars(Car* head) {
    int swapped, i;
    Car *car;
    Car *lCar = nullptr;

    if (head == nullptr) {
        return;
    }

    do {
        swapped = 0;
        car = head;

        while (car->getNext() != lCar) {
            if (*car > *(car->getNext())) {
                swap(car, car->getNext());
                swapped = 1;
            }

            car = car->getNext();
        }
        lCar = car;
    }
    while (swapped);
}

void Race::sortCars(Car* head) const {
    int swapped, i;
    Car *car;
    Car *lCar = nullptr;

    if (head == nullptr) {
        return;
    }

    do {
        swapped = 0;
        car = head;

        while (car->getNext() != lCar) {
            if (*car > *(car->getNext())) {
                swap(car, car->getNext());
                swapped = 1;
            }

            car = car->getNext();
        }
        lCar = car;
    }
    while (swapped);
}

void Race::swap(Car* l, Car* r) {
    std::string tempDriverName = l->getDriverName();
    double tempPerformance = l->getPerformance();
    Laptime* tempHead = l->getHead();

    l->setDriverName(r->getDriverName());
    l->setPerformance(r->getPerformance());
    l->setHead(r->getHead());

    r->setDriverName(tempDriverName);
    r->setPerformance(tempPerformance);
    r->setHead(tempHead);
}

void Race::swap(Car* l, Car* r) const {
    std::string tempDriverName = l->getDriverName();
    double tempPerformance = l->getPerformance();
    Laptime* tempHead = l->getHead();

    l->setDriverName(r->getDriverName());
    l->setPerformance(r->getPerformance());
    l->setHead(r->getHead());

    r->setDriverName(tempDriverName);
    r->setPerformance(tempPerformance);
    r->setHead(tempHead);
}



std::ostream& operator<<(std::ostream& os, const Race& race) {
    race.sortCars(race.getHead());
    
    Car *currentCar = race.getHead();
    int numberCar = 0, count = 0, fastestLapPos = 0, lenPos, fastestTime, f;

    while (currentCar != nullptr) {
        currentCar = currentCar->getNext();
        numberCar++;
    }

    if (numberCar < 2) {return os;}

    lenPos = (log(numberCar) / log(10)) + 1;

    currentCar = race.getHead();
    fastestTime = currentCar->getFastestLaptime()->getLaptime();

    while (currentCar != nullptr) {
        if (fastestTime > currentCar->getFastestLaptime()->getLaptime()) {
            fastestTime = currentCar->getFastestLaptime()->getLaptime();
            fastestLapPos = count;
        }

        count++;
        currentCar = currentCar->getNext();
    }

    currentCar = race.getHead();
    count = 0;
    std::string pos;
    std::string addPoints[10] = {"25", "18", "15", "12", "10", "08", "06", "04", "02", "01"};

    while (count < numberCar) {
        pos = std::to_string(count + 1);
        while (pos.size() != lenPos) {
            pos = "0" + pos;
        }

        std::cout << pos + "--";
        std::cout << *currentCar;
        if (count < 10) {
            std::cout << "--" + addPoints[count];
            
            if (count == fastestLapPos) {
                std::cout <<  "--1";
            }
        }

        if (count + 1 != numberCar) {
            std::cout << "\n";
        }

        currentCar = currentCar->getNext();
        count++;
    }

    return os;
}



