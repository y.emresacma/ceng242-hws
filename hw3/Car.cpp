#include "Car.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE 
*/

Car::Car(std::string driver_name) {
    this->driver_name = driver_name;
    this->performance = Utilizer::generatePerformance();
    this->head = nullptr;
    this->next = nullptr;
}

Car::Car(const Car& rhs) {
    this->driver_name = rhs.getDriverName();
    this->performance = rhs.getPerformance(); 
    this->next = nullptr;


    if (rhs.getHead() == nullptr) {
        this->head = nullptr;
    }

    else {
        this->head = new Laptime(rhs.getHead()->getLaptime());
        Laptime *current = this->head;
        Laptime *rhsHead = rhs.getHead();
        Laptime *currentRhs = rhsHead;

        while (currentRhs->getNext() != nullptr) {
            current->setNext(new Laptime(currentRhs->getNext()->getLaptime()));
            currentRhs = currentRhs->getNext();
            current = current->getNext();
        }
    }

    Car *rhsCar = rhs.getNext();
    Car *current = this;

    while (rhsCar != nullptr) {
        current->setNext(new Car(*rhsCar));
        rhsCar = rhsCar->getNext();
        current = current->getNext();
    }


}

Car::~Car() {
    Laptime* current = this->head, *temp;

    while (current != nullptr) {
        temp = current->getNext();
        delete current;
        current = temp;
    }
    this->head = nullptr;
}

std::string Car::getDriverName() const{
    return this->driver_name;
}

std::string Car::getDriverName(){
    return this->driver_name;
}

double Car::getPerformance() const {
    return this->performance;
}

double Car::getPerformance() {
    return this->performance;
}

 void Car::setPerformance(double performance) {
     this->performance = performance;
 }

 void Car::setDriverName(std::string name) {
     this->driver_name = name;
 }

 Laptime* Car::getHead() {
     return this->head;
 }

  Laptime* Car::getHead() const {
     return this->head;
 }

void Car::setHead(Laptime* head) {
    this->head = head;
}

Car* Car::getNext() {
    return this->next;
}

Car* Car::getNext() const {
    return this->next;
}

void Car::setNext(Car* next) {
    this->next = next;
}

void Car::addCar(Car* next) {
    Car* n = this;

    while (n->getNext() != nullptr) {
        n = n->getNext();
    }

    n->setNext(next);
}

bool Car::operator<(const Car& rhs) const {
    int lTime = 0, rTime = 0;
    Laptime* laptime;

    laptime = this->head;
    while (laptime != nullptr) {
        lTime += laptime->getLaptime();
        laptime = laptime->getNext();
    }

    laptime = rhs.getHead();
    while (laptime != nullptr) {
        rTime += laptime->getLaptime();
        laptime = laptime->getNext();
    }

    return lTime < rTime;
}

bool Car::operator>(const Car& rhs) const {
    int lTime = 0, rTime = 0;
    Laptime* laptime;

    laptime = this->head;
    while (laptime != nullptr) {
        lTime += laptime->getLaptime();
        laptime = laptime->getNext();
    }

    laptime = rhs.getHead();
    while (laptime != nullptr) {
        rTime += laptime->getLaptime();
        laptime = laptime->getNext();
    }

    return lTime > rTime;
}

Laptime Car::operator[](const int lap) const {
    int count = 0;
    Laptime *currentLaptime = this->head;

    while (count++ < lap) {
        if (currentLaptime == nullptr) {
            return *(new Laptime(0));
        }

        currentLaptime = currentLaptime->getNext();
    }

    return *currentLaptime;
}

Laptime* Car::getLatestLaptime() const {
    Laptime* l = this->head;

    while (l != nullptr && l->getNext() != nullptr) {
        l = l->getNext();
    }

    return l;
}

Laptime* Car::getTotalLaptime() const {
    int totalTime = 0;
    Laptime* currentLap = this->head;

    while (currentLap != nullptr) {
        totalTime += currentLap->getLaptime();
        currentLap = currentLap->getNext();
    }

    return (new Laptime(totalTime));
}

Laptime* Car::getFastestLaptime() const {
    Laptime* currentLap = this->head;
    Laptime* fastestLap = currentLap;

    while (currentLap != nullptr) {
        if (fastestLap->getLaptime() > currentLap->getLaptime()) {
            fastestLap = currentLap;
        }

        currentLap = currentLap->getNext();
    }

    return fastestLap;
}

void Car::Lap(const Laptime& average_laptime) {
    int variance = Utilizer::generateLaptimeVariance(this->performance);
    
    if (this->head == nullptr) {
        this->head = new Laptime(variance + average_laptime.getLaptime());
        return;
    }

    this->head->addLaptime(new Laptime(variance + average_laptime.getLaptime()));
}

std::ostream& operator<<(std::ostream& os, const Car& car) {
    std::string name = car.getDriverName();
    std::string surname = name.substr(1 + name.find_last_of(' ')).substr(0, 3);
        std::for_each(surname.begin(), surname.end(), [](char & c){
            c = ::toupper(c);
        });

    Laptime* latestLaptime = car.getLatestLaptime();
    Laptime* fastestLaptime = car.getFastestLaptime();
    Laptime* totalLaptime = car.getTotalLaptime();

    std::cout << surname << "--"
        << *latestLaptime << "--"
        << *fastestLaptime << "--"
        << *totalLaptime;

    delete totalLaptime;
    return os;
}