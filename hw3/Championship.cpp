#include "Championship.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE 
*/

Championship::Championship() {
   
}

Championship::Championship(const Championship& rhs) : races(rhs.getRaces()) {

}

Championship::~Championship() {
}

std::vector< Race > & Championship::getRaces() {
    return this->races;
}   

std::vector< Race > Championship::getRaces() const {
    return this->races;
}

void Championship::addNewRace(Race& race) {
    this->races.push_back(race);
    Race *x = &this->getRaces()[0];
    

    Car *currentCar = race.getHead();
    Laptime* currentLaptime, *vectorLap;
    Car* vectorCar = x->getHead();

    while (currentCar != nullptr) {
        currentLaptime = currentCar->getHead();
        vectorLap = vectorCar->getHead();

        if (currentLaptime != nullptr) {
            vectorCar->setHead(new Laptime(currentLaptime->getLaptime()));
            vectorLap = vectorCar->getHead();
            currentLaptime = currentLaptime->getNext();

            while(currentLaptime != nullptr) {
                vectorLap->addLaptime(new Laptime(currentLaptime->getLaptime()));
                currentLaptime = currentLaptime->getNext();        
            }
        }
        currentCar = currentCar->getNext();
        vectorCar = vectorCar->getNext();

    }    

}

void Championship::addNewRace(std::string race_name) {
    Race *temp = &this->races[0];
    
    Car* tempCar = temp->getHead();
    Car* vecCar = this->races[0].getHead();
    Laptime *tempLap, *vecLap;
    while(vecCar != nullptr) {
        tempLap = tempCar->getHead();
        vecLap = vecCar->getHead();

        if (vecLap != NULL) {
            tempCar->setHead(new Laptime(vecLap->getLaptime()));
            tempLap = tempCar->getHead();
            vecLap = vecLap->getNext();

            while (vecLap != NULL) {
                tempLap->addLaptime(new Laptime(vecLap->getLaptime()));
                vecLap = vecLap->getNext();
            }
        }
        
        vecCar = vecCar->getNext();
        tempCar = tempCar->getNext();
    }
    
    Race r(this->races[0]);
    r.setRaceName(race_name);


    this->races.push_back(r);

    
    this->races[0] = *temp;
    

}

void Championship::removeRace(std::string race_name) {
    for (int i = 0; i < this->races.size(); i++) {
        if (this->races[i].getRaceName() == race_name) {
            this->races.erase(this->races.begin() + i);
            break;
        }
    }
}

void Championship::addLap(std::string race_name) {
    int count;
    for (std::vector<Race>::iterator it = this->races.begin(); it != this->races.end(); ++it) {
        if (it->getRaceName() == race_name) {
            it->operator++();
            break;
        }
    }

}



Race & Championship::operator[](std::string race_name) {
    for (int i = 0; i < this->races.size(); i++) {
        if (this->races[i].getRaceName() == race_name) {
            return this->races[i];
        }
    }
}

std::ostream& operator<<(std::ostream& os, const Championship& championship) {
    std::map<std::string, int> result;
    int addPoints[10] = {25, 18, 15, 12, 10, 8, 6, 4, 2, 1};
    int numberCars = 0, lenPos, fastestTime, count, pos;
    std::string fastestDriver, surname, posStr;
    char z;

    Race temp("temp");
    temp = (championship.races[0]);
    Race *currentRace;
    currentRace = &temp;

    Car *c = currentRace->getHead(), *currentCar;

    while (c != nullptr) {
        result[c->getDriverName()] = 0;
        c = c->getNext();
        numberCars++;
    }
    

    if (numberCars == 0) {return os;}
    lenPos = (log(numberCars) / log(10)) + 1;

    for (int i = 0; i < championship.races.size(); i++) {
        temp = championship.races[i];
        currentRace = &temp;
        currentCar = currentRace->getHead();
        
        fastestTime = currentCar->getFastestLaptime()->getLaptime();
        fastestDriver = currentCar->getDriverName();

        while (currentCar != nullptr) {
            if (currentCar->getFastestLaptime()->getLaptime() < fastestTime) {
                fastestTime = currentCar->getFastestLaptime()->getLaptime();
                fastestDriver = currentCar->getDriverName();
            }

            currentCar = currentCar->getNext();
        }

        currentCar = currentRace->getHead();
        count = 0;

        while (currentCar != nullptr) {
            if (count < 10) {
                result[currentCar->getDriverName()] += addPoints[count];

                if (currentCar->getDriverName() == fastestDriver) {
                    result[currentCar->getDriverName()]++;
                }
            }
            else {
                break;
            }

            count++;
            currentCar = currentCar->getNext();
        }
    }

    typedef std::function<bool(std::pair<std::string, int>, std::pair<std::string, int>)> Comparator;
	Comparator compFunctor = [](std::pair<std::string, int> elem1 ,std::pair<std::string, int> elem2)
			{
				return elem1.second >= elem2.second;
			};

    std::set<std::pair<std::string, int>, Comparator> setOfWords(
			result.begin(), result.end(), compFunctor);


    pos = 1;

    for (std::pair<std::string, int> element : setOfWords) {
        posStr = std::to_string(pos++);
        while(posStr.size() != lenPos) {
            posStr = "0" + posStr;
        }

        surname = element.first.substr(1 + element.first.find_last_of(' ')).substr(0, 3);

        std::for_each(surname.begin(), surname.end(), [](char & c){
            c = ::toupper(c);
        });

        std::cout << posStr << "--" << surname << "--" << element.second << "\n";
    }

    return os;
}
