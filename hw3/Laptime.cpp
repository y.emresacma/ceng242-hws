#include "Laptime.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE 
*/
Laptime::Laptime(int laptime) {
    this->laptime = laptime;
    this->next = nullptr;
}

Laptime::Laptime(const Laptime& rhs) {
    this->laptime = rhs.getLaptime();
    this->next = nullptr;
}

Laptime::~Laptime() {

}

void Laptime::addLaptime(Laptime *next) {
    Laptime* l = this;

    while (l->getNext() != nullptr) {
        l = l->getNext();
    }

    l->setNext(next);
}

bool Laptime::operator<(const Laptime& rhs) const {
    if (this->laptime < rhs.getLaptime()) {
        return true;
    }

    return false;
}

bool Laptime::operator>(const Laptime& rhs) const {
    if (this->laptime > rhs.getLaptime()) {
        return true;
    }

    return false;
}

Laptime& Laptime::operator+(const Laptime& rhs) {
    this->setLaptime(this->laptime + rhs.getLaptime());
    return *this;
}

std::ostream& operator<<(std::ostream& os, const Laptime& laptime) {    
    int intMinutes = laptime.laptime / 60000;
    int intSecond = (laptime.laptime - intMinutes * 60000) / 1000;

    std::string minutes = std::to_string(intMinutes);
    std::string second = std::to_string(intSecond);
    std::string miliSecond = std::to_string(laptime.laptime - intMinutes * 60000 - intSecond * 1000);

    while (second.size() < 2) {
        second = "0" + second;
    }

    while (miliSecond.size() < 3) {
        miliSecond = "0" + miliSecond;
    }

    std::cout << minutes << ":" << second << "." << miliSecond;
    return os;
}


void Laptime::setLaptime(int laptime) {
    this->laptime = laptime;
}

void Laptime::setNext(Laptime* laptime) {
    this->next = laptime;
}

int Laptime::getLaptime() {
    return this->laptime;
}

int Laptime::getLaptime() const {
    return this->laptime;
}

Laptime* & Laptime::getNext() {
    return this->next;
}

Laptime* Laptime::getNext() const{
    return this->next;
}