#include"Fighter.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE
*/

Fighter::Fighter(uint id, int x, int y, Team team) :
    Player(id, x, y, team)
    {
        this->attackDamage = 100;
        this->healPower = 0;
        this->maxHP = 400;
        this->HP = 400;
        
        this->goals.push_back(ATTACK);
        this->goals.push_back(TO_ENEMY);
        this->goals.push_back(CHEST);
        
        if (this->team == BARBARIANS)
        {
            this->abbrevation = "FI";
        }
        else
        {
            this->abbrevation = "fi";
        }
    }

int Fighter::getAttackDamage() const
{
    return this->attackDamage;
}

int Fighter::getHealPower() const
{
    return this->healPower;
}

int Fighter::getMaxHP() const
{
    return this->maxHP;
}

vector<Goal> Fighter::getGoalPriorityList()
{
    return this->goals;
}

const string Fighter::getClassAbbreviation() const
{
    return this->abbrevation;
}

vector<Coordinate> Fighter::getAttackableCoordinates()
{
    vector<Coordinate> attackableCoordinates;

    if (this->coordinate.x >= 1)
    {
        attackableCoordinates.push_back(Coordinate(this->coordinate.x - 1, this->coordinate.y));
    }

    if (this->coordinate.y >= 1)
    {
        attackableCoordinates.push_back(Coordinate(this->coordinate.x, this->coordinate.y - 1));        
    }

    if (this->coordinate.x <= this->boardSize - 2)
    {
        attackableCoordinates.push_back(Coordinate(this->coordinate.x + 1, this->coordinate.y));
    }

    if (this->coordinate.y <= this->boardSize - 2)
    {
        attackableCoordinates.push_back(Coordinate(this->coordinate.x, this->coordinate.y + 1));
    }

    return attackableCoordinates;
}

vector<Coordinate> Fighter::getMoveableCoordinates()
{    vector<Coordinate> moveableCoordinates;

    if (this->coordinate.x >= 1)
    {
        moveableCoordinates.push_back(Coordinate(this->coordinate.x - 1, this->coordinate.y));
    }

    if (this->coordinate.y >= 1)
    {
        moveableCoordinates.push_back(Coordinate(this->coordinate.x, this->coordinate.y - 1));        
    }

    if (this->coordinate.x <= this->boardSize - 2)
    {
        moveableCoordinates.push_back(Coordinate(this->coordinate.x + 1, this->coordinate.y));
    }

    if (this->coordinate.y <= this->boardSize - 2)
    {
        moveableCoordinates.push_back(Coordinate(this->coordinate.x, this->coordinate.y + 1));
    }

    return moveableCoordinates; 
}

vector<Coordinate> Fighter::getHealableCoordinates()
{
    return vector<Coordinate>();
}