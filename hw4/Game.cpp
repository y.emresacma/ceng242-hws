#include"Game.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE
*/

Game::Game(uint maxTurnNumber, uint BoardSize, Coordinate chest) :
    board(Board(BoardSize, &this->players, chest)),
    maxTurnNumber(maxTurnNumber),
    turnNumber(0)
    {
    }

Game::~Game()
{
    for (Player *player : this->players)
    {
        delete player;
        player = nullptr;
    }

    this->players.clear();
}

void Game::addPlayer(int id, int x, int y, Team team, std::string cls)
{
    if (cls == "ARCHER")
    {
        this->players.push_back(new Archer(id, x, y, team));
    }
    else if (cls == "FIGHTER")
    {
        this->players.push_back(new Fighter(id, x, y, team));
    }
    else if (cls == "PRIEST")
    {
        this->players.push_back(new Priest(id, x, y, team));
    }
    else if (cls == "SCOUT")
    {
        this->players.push_back(new Scout(id, x, y, team));
    }
    else if (cls == "TANK")
    {
        this->players.push_back(new Tank(id, x, y, team));
    }
}

bool Game::isGameEnded()
{
    int barbarian = 0;
    int knight = 0;

    if (this->maxTurnNumber == this->turnNumber)
    {
        cout << "Game is ended at turn "
             << this->turnNumber
             << ". Maximum turn number reached. Knight victory.\n";
        
        return true;
    }

    for (Player *player : this->players)
    {
        if (player->getTeam() == BARBARIANS) 
        {
            barbarian = 1;
            
            if (player->getCoord() == this->board.getChestCoordinates())
            {
                cout << "Game ended at turn "
                     << this->turnNumber
                     << ". Chest captured. Barbarian victory.\n";

                return true;
            }
        }
        else
        {
            knight = 1;
        }
    }

    if (knight == 1 && barbarian == 0)
    {
        cout << "Game ended at turn "
             << this->turnNumber
             << ". All barbarians dead. Knight victory.\n";

        return true;
    }
    else if (knight == 0 && barbarian == 1)
    {
        cout << "Game ended at turn "
             << this->turnNumber
             << ". All knights dead. Barbarian victory.\n";

        return true;
    }

    return false;
}

void Game::playTurn()
{
    sort(this->players.begin(), this->players.end(), 
        [](const Player & a, const Player & b) -> bool
    {
        return a.getID() < b.getID();
    });

    cout << "Turn "
            << this->turnNumber
            << " has started.\n";

    for (Player *player : this->players)
    {
        this->playTurnForPlayer(player);
    }
}

Goal Game::playTurnForPlayer(Player *player)
{
    if (player->getHP() <= 0)
    {
        cout << "Player "
             << player->getBoardID()
             << " died.";

        for (auto it = this->players.begin(); it != this->players.end(); it++)
        {
            if ((*it)->getID() == player->getID())
            {
                this->players.erase(it);
                break;
            }
        }

        delete player;
        player = nullptr;
    
        return NO_GOAL;
    }

    vector<Coordinate> validCoordinates;
    Player* targetPlayer = nullptr;

    for (Goal goal : player->getGoalPriorityList())
    {  
        if (goal == ATTACK)
        {
            validCoordinates = player->getAttackableCoordinates();
            
            for (Player *current : this->players)
            {
                for (Coordinate c : validCoordinates)
                {
                    if (c == current->getCoord() && player->isEnemy(current))
                    {
                        if (targetPlayer == nullptr)
                        {
                            targetPlayer = current;
                        }
                        else if (targetPlayer->getID() > current->getID())
                        {
                            targetPlayer = current;
                        }
                    }
                }
            }
            
            if (targetPlayer != nullptr)
            {
                player->attack(targetPlayer);
                return ATTACK;
            }

            validCoordinates.clear();
        }
        else if (goal == CHEST)
        {
            int up = 0, right = 0;

            if (this->board.getChestCoordinates().x > player->getCoord().x)
            {
                right = 1;
            }
            if (this->board.getChestCoordinates().y > player->getCoord().x)
            {
                up = 1;
            }
            
            if (up == 1)
            {
                Coordinate row(player->getCoord().x, player->getCoord().y + 1);
                if (!(this->board.isPlayerOnCoordinate(row)))
                {
                    player->movePlayerToCoordinate(row);
                    return CHEST;
                }
            }
            else
            {
                Coordinate row(player->getCoord().x, player->getCoord().y - 1);
                if (!(this->board.isPlayerOnCoordinate(row)))
                {
                    player->movePlayerToCoordinate(row);
                    return CHEST;
                }
            }

            if (right == 1)
            {
                Coordinate column(player->getCoord().x + 1, player->getCoord().y);
                if (!(this->board.isPlayerOnCoordinate(column)))
                {
                    player->movePlayerToCoordinate(column);
                    return CHEST;
                }
            }
            else
            {
                Coordinate column(player->getCoord().x - 1, player->getCoord().y);
                if (!(this->board.isPlayerOnCoordinate(column)))
                {
                    player->movePlayerToCoordinate(column);
                    return CHEST;
                }
            }
        }
        else if (goal == TO_ENEMY)
        {
            Player *nearestEnemy = nullptr;
            int up = 0, right = 0;
            int distance = 0, tempDistance;

            for (Player *current : this->players)
            {
                if (player->isEnemy(current))
                {
                    if (nearestEnemy == nullptr)
                    {
                        nearestEnemy = current;
                        distance = current->getCoord() - player->getCoord();
                    }
                    else
                    {
                        tempDistance = current->getCoord() - player->getCoord();
                        if (tempDistance < distance)
                        {
                            distance = tempDistance;
                            nearestEnemy = current;
                        }
                        else if (tempDistance == distance)
                        {
                            if (current->getID() < nearestEnemy->getID())
                            {
                                nearestEnemy = current;
                            }
                        }
                    }
                }
            }

            if (nearestEnemy->getCoord().x > player->getCoord().x)
            {
                right = 1;
            }
            if (nearestEnemy->getCoord().y > player->getCoord().x)
            {
                up = 1;
            }

            if (up == 1)
            {
                Coordinate column(player->getCoord().x, player->getCoord().y + 1);
                if (!(this->board).isPlayerOnCoordinate(column))
                {
                    player->movePlayerToCoordinate(column);
                    return TO_ENEMY;
                }
            }
            else
            {
                Coordinate column(player->getCoord().x, player->getCoord().y - 1);
                if (!(this->board).isPlayerOnCoordinate(column))
                {
                    player->movePlayerToCoordinate(column);
                    return TO_ENEMY;
                }
            }

            if (right == 1)
            {
                Coordinate row(player->getCoord().x + 1, player->getCoord().y);
                if (!(this->board).isPlayerOnCoordinate(row))
                {
                    player->movePlayerToCoordinate(row);
                    return TO_ENEMY;
                }
            }
            else
            {
                Coordinate row(player->getCoord().x - 1, player->getCoord().y);
                if (!(this->board).isPlayerOnCoordinate(row))
                {
                    player->movePlayerToCoordinate(row);
                    return TO_ENEMY;
                }
            }
        }
        else if (goal == TO_ALLY)
        {
            Player *nearestAlly = nullptr;
            int up = 0, right = 0;
            int distance = 0, tempDistance;

            for (Player *current : this->players)
            {
                if (!(player->isEnemy(current)) && current->getID() != player->getID())
                {
                    if (nearestAlly == nullptr)
                    {
                        nearestAlly = current;
                        distance = current->getCoord() - player->getCoord();
                    }
                    else
                    {
                        tempDistance = current->getCoord() - player->getCoord();
                        if (tempDistance < distance)
                        {
                            distance = tempDistance;
                            nearestAlly = current;
                        }
                        else if (tempDistance == distance)
                        {
                            if (current->getID() < nearestAlly->getID())
                            {
                                nearestAlly = current;
                            }
                        }
                    }
                }
            }

            if (nearestAlly->getCoord().x > player->getCoord().x)
            {
                right = 1;
            }
            if (nearestAlly->getCoord().y > player->getCoord().x)
            {
                up = 1;
            }

            if (up == 1)
            {
                Coordinate column(player->getCoord().x, player->getCoord().y + 1);
                if (!(this->board).isPlayerOnCoordinate(column))
                {
                    player->movePlayerToCoordinate(column);
                    return TO_ENEMY;
                }
            }
            else
            {
                Coordinate column(player->getCoord().x, player->getCoord().y - 1);
                if (!(this->board).isPlayerOnCoordinate(column))
                {
                    player->movePlayerToCoordinate(column);
                    return TO_ENEMY;
                }
            }

            if (right == 1)
            {
                Coordinate row(player->getCoord().x + 1, player->getCoord().y);
                if (!(this->board).isPlayerOnCoordinate(row))
                {
                    player->movePlayerToCoordinate(row);
                    return TO_ENEMY;
                }
            }
            else
            {
                Coordinate row(player->getCoord().x - 1, player->getCoord().y);
                if (!(this->board).isPlayerOnCoordinate(row))
                {
                    player->movePlayerToCoordinate(row);
                    return TO_ENEMY;
                }
            }
        }
        else if (goal == HEAL)
        {
            validCoordinates = player->getHealableCoordinates();
            int check = 0;

            for (Coordinate c : validCoordinates)
            {
                for (Player *current : this->players)
                {
                    if (!(player->isEnemy(current)) && c == current->getCoord())
                    {
                        player->heal(current);
                        check = 1;
                    }
                }
            }

            if (check == 1)
            {
                return HEAL;
            }
        }
    }

    return NO_GOAL;
}

