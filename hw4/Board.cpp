#include"Board.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE
*/

Board::Board(uint size, std::vector<Player*>* players, Coordinate chest) :
    size(size),
    players(players),
    chest(chest)
    {
    }

Board::~Board()
{
    for (Player *player : *(this->players))
    {
        delete player;
        player = nullptr;
    }

    delete this->players;
    this->players = nullptr;
}

bool Board::isCoordinateInBoard(const Coordinate& c)
{
    if (c < Coordinate(this->size, this->size) && !(c < Coordinate(0, 0)))
    {
        return true;
    }
    
    return false;
}

bool Board::isPlayerOnCoordinate(const Coordinate& c)
{
    for (Player *player : *(this->players))
    {
        if (player->getCoord() == c)
        {
            return true;
        }
    }

    return false;   
}

Player *Board::operator[](const Coordinate&c)
{
    for (Player *player : *(this->players))
    {
        if (player->getCoord() == c)
        {
            return player;
        }
    }

    return nullptr;
}

Coordinate Board::getChestCoordinates()
{
    return this->chest;
}

void Board::printBoardwithID()
{
    string board[this->size][this->size];

    for (auto& row : board)
    {
        for (auto& elem : row)
        {
            elem = "__";
        }
    }

    for (Player *player : *(this->players))
    {
        Coordinate c(player->getCoord());
        board[c.x][c.y] = player->getBoardID();
    }

    if (board[this->chest.x][this->chest.y] == "__")
    {
        board[this->chest.x][this->chest.y] = "Ch";
    }

    for (auto& row : board)
    {
        for (auto& elem : row)
        {
            cout << elem + " ";
        }
        cout << "\n";
    }
}

void Board::printBoardwithClass()
{
    string board[this->size][this->size];

    for (auto& row : board)
    {
        for (auto& elem : row)
        {
            elem = "__";
        }
    }

    for (Player *player : *(this->players))
    {
        Coordinate c(player->getCoord());
        board[c.x][c.y] = player->getClassAbbreviation();
    }

    if (board[this->chest.x][this->chest.y] == "__")
    {
        board[this->chest.x][this->chest.y] = "Ch";
    }

    for (auto& row : board)
    {
        for (auto& elem : row)
        {
            cout << elem + " ";
        }
        cout << "\n";
    }
}