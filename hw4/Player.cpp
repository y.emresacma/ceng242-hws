#include"Player.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE
*/

Player::Player(uint id, int x, int y, Team team) :
    id(id), team(team), coordinate(Coordinate(x, y))
    {
    }

uint Player::getID() const
{
    return this->id;
}

const Coordinate& Player::getCoord() const
{
    return this->coordinate;
}

int Player::getHP() const
{
    return this->HP;
}

Team Player::getTeam() const
{
    return this->team;
}

std::string Player::getBoardID()
{
    return (this->id > 9) ? to_string(this->id) : ("0" + to_string(this->id));
}

bool Player::attack(Player *enemy) 
{
    enemy->setHP(enemy->getHP() - this->attackDamage);

    cout 
        << "Player "
        << this->getBoardID()
        << " attacked Player "
        << enemy->getBoardID()
        << "(" + to_string(this->attackDamage) + ")"
        << endl;
    
    return (enemy->getHP() > 0) ? true : false;
}

void Player::heal(Player *ally)
{
    ally->setHP(max(ally->getHP() + this->healPower, ally->getMaxHP()));

    cout 
        << "Player "
        << this->getBoardID()
        << " healed Player "
        << ally->getBoardID();
}

void Player::movePlayerToCoordinate(Coordinate c)
{
    if (c.x >= 0 && c.y >= 0 && c.x <= (this->boardSize - 1) && c.y <= (this->boardSize - 1))
    {
        this->coordinate = c;
    }
}

bool Player::isDead() const
{
    return this->HP <= 0;
}

void Player::setHP(int HP)
{
    this->HP = HP;
}

int Player::getAttackDamage()
{
    return this->attackDamage;
}

int Player::getBoardSize()
{
    return this->boardSize;
}

bool Player::isEnemy(Player* player)
{
    return (player->getTeam() != this->team);
}

void Player::setBoardSize(int size)
{
    this->boardSize = size;
}

bool Player::operator<(const Player& player) const
{
    return (this->id < player.getID());
}
