#include"Priest.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE
*/

Priest::Priest(uint id, int x, int y, Team team) :
    Player(id, x, y, team)
    {
        this->attackDamage = 0;
        this->healPower = 50;
        this->maxHP = 150;
        this->HP = 150;
        
        this->goals.push_back(HEAL);
        this->goals.push_back(TO_ALLY);
        this->goals.push_back(CHEST);

        if (team == BARBARIANS)
        {
            this->abbrevation = "PR";
        }
        else
        {
            this->abbrevation = "pr";
        }
    }

int Priest::getAttackDamage() const
{
    return this->attackDamage;
}

int Priest::getHealPower() const
{
    return this->healPower;
}

int Priest::getMaxHP() const
{
    return this->maxHP;
}

std::vector<Goal> Priest::getGoalPriorityList()
{
    return this->goals;
}

const string Priest::getClassAbbreviation() const
{
    return this->abbrevation;
}

vector<Coordinate> Priest::getAttackableCoordinates()
{
     return vector<Coordinate>();   
}

vector<Coordinate> Priest::getMoveableCoordinates()
{
    vector<Coordinate> moveableCoordinate;

    for (int i = this->coordinate.x - 1; i <= this->coordinate.x + 1; i++)
    {
        for (int j = this->coordinate.y -1; j <= this->coordinate.y + 1; j++)
        {
            if (i >= 0 && i <= this->boardSize - 1 && j >= 0 && j <= this->boardSize - 1)
            {
                if (this->coordinate.x != i && this->coordinate.y != j)
                {
                    moveableCoordinate.push_back(Coordinate(i ,j));
                }
            }
        }
    }

    return moveableCoordinate;
}

vector<Coordinate> Priest::getHealableCoordinates()
{
    vector<Coordinate> healableCoordinates;

    for (int i = this->coordinate.x - 1; i <= this->coordinate.x + 1; i++)
    {
        for (int j = this->coordinate.y -1; j <= this->coordinate.y + 1; j++)
        {
            if (i >= 0 && i <= this->boardSize - 1 && j >= 0 && j <= this->boardSize - 1)
            {
                if (this->coordinate.x != i && this->coordinate.y != j)
                {
                    healableCoordinates.push_back(Coordinate(i ,j));
                }
            }
        }
    }

    return healableCoordinates;
}