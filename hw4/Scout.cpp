#include"Scout.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE
*/

Scout::Scout(uint id, int x, int y, Team team) :
    Player(id, x, y, team)
    {
        this->attackDamage = 25;
        this->healPower = 0;
        this->maxHP = 125;
        this->HP = 125;
        
        this->goals.push_back(CHEST);
        this->goals.push_back(TO_ALLY);
        this->goals.push_back(ATTACK);
        
        if (this->team == BARBARIANS)
        {
            this->abbrevation = "SC";
        }
        else
        {
            this->abbrevation = "sc";
        }
    }

int Scout::getAttackDamage() const
{
    return this->attackDamage;
}

int Scout::getHealPower() const
{
    return this->healPower;
}

int Scout::getMaxHP() const
{
    return this->maxHP;
}

std::vector<Goal> Scout::getGoalPriorityList()
{
    return this->goals;
}

const string Scout::getClassAbbreviation() const
{
    return this->abbrevation;
}

vector<Coordinate> Scout::getAttackableCoordinates()
{
    vector<Coordinate> attackableCordinate;

    for (int i = this->coordinate.x - 1; i <= this->coordinate.x + 1; i++)
    {
        for (int j = this->coordinate.y -1; j <= this->coordinate.y + 1; j++)
        {
            if (i >= 0 && i <= this->boardSize - 1 && j >= 0 && j <= this->boardSize - 1)
            {
                if (this->coordinate.x != i && this->coordinate.y != j)
                {
                    attackableCordinate.push_back(Coordinate(i ,j));
                }
            }
        }
    }

    return attackableCordinate;
}

vector<Coordinate> Scout::getMoveableCoordinates()
{
    vector<Coordinate> moveableCoordinates;

    for (int i = this->coordinate.x - 1; i <= this->coordinate.x + 1; i++)
    {
        for (int j = this->coordinate.y -1; j <= this->coordinate.y + 1; j++)
        {
            if (i >= 0 && i <= this->boardSize - 1 && j >= 0 && j <= this->boardSize - 1)
            {
                if (this->coordinate.x != i && this->coordinate.y != j)
                {
                    moveableCoordinates.push_back(Coordinate(i ,j));
                }
            }
        }
    }

    return moveableCoordinates;
}

vector<Coordinate> Scout::getHealableCoordinates()
{
    return vector<Coordinate>();
}