#include"Archer.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE
*/

Archer::Archer(uint id, int x, int y, Team team) : 
    Player(id, x, y, team)
    {
      this->attackDamage = 50;
      this->healPower = 0;
      this->maxHP = 200;
      this->goals.push_back(ATTACK);

      if (team == BARBARIANS)
      {
          this->abbrevation = "AR";
      }
      else
      {
          this->abbrevation = "ar";
      }
    }

int Archer::getAttackDamage() const
{
    return this->attackDamage;
}

int Archer::getHealPower() const
{
    return this->healPower;
}

int Archer::getMaxHP() const
{
    return this->maxHP;
}

std::vector<Goal> Archer::getGoalPriorityList()
{
    return this->goals;
}

const string Archer::getClassAbbreviation() const
{
    return this->abbrevation;
}

vector<Coordinate> Archer::getAttackableCoordinates()
{
    vector<Coordinate> attackableCoordinates;

    for (int i = this->coordinate.x - 2; i <= this->coordinate.x + 2; i++)
    {
        if (i != this->coordinate.x && i >= 0 && i <= this->boardSize - 1)
        {
            attackableCoordinates.push_back(Coordinate(i, this->coordinate.y));
        }
    }

    for (int j = this->coordinate.y - 2; j <= this->coordinate.y + 2; j++)
    {
        if (j != this->coordinate.y && j >= 0 && j <= this->boardSize - 1)
        {
            attackableCoordinates.push_back(Coordinate(this->coordinate.x, j));
        }
    }

    return attackableCoordinates;
}

vector<Coordinate> Archer::getMoveableCoordinates()
{
    return vector<Coordinate>();
}

vector<Coordinate> Archer::getHealableCoordinates()
{
    return vector<Coordinate>();
}