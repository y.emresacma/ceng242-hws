#include"Tank.h"

/*
YOU MUST WRITE THE IMPLEMENTATIONS OF THE REQUESTED FUNCTIONS
IN THIS FILE. START YOUR IMPLEMENTATIONS BELOW THIS LINE
*/
Tank::Tank(uint id, int x, int y, Team team) :
    Player(id, x, y, team)
    {
        this->attackDamage = 25;
        this->healPower = 0;
        this->maxHP = 1000;
        this->HP = 1000;
        
        this->goals.push_back(TO_ENEMY);
        this->goals.push_back(ATTACK);
        this->goals.push_back(CHEST);
        
        if (this->team == BARBARIANS)
        {
            this->abbrevation = "TA";
        }
        else
        {
            this->abbrevation = "ta";
        }
    }

int Tank::getAttackDamage() const
{
    return this->attackDamage;
}

int Tank::getHealPower() const
{
    return this->healPower;
}

int Tank::getMaxHP() const
{
    return this->maxHP;
}

std::vector<Goal> Tank::getGoalPriorityList()
{
    return this->goals;
}

const string Tank::getClassAbbreviation() const
{
    return this->abbrevation;
}

vector<Coordinate> Tank::getAttackableCoordinates()
{
    vector<Coordinate> attackableCoordinates;

    if (this->coordinate.x >= 1)
    {
        attackableCoordinates.push_back(Coordinate(this->coordinate.x - 1, this->coordinate.y));
    }

    if (this->coordinate.y >= 1)
    {
        attackableCoordinates.push_back(Coordinate(this->coordinate.x, this->coordinate.y - 1));        
    }

    if (this->coordinate.x <= this->boardSize - 2)
    {
        attackableCoordinates.push_back(Coordinate(this->coordinate.x + 1, this->coordinate.y));
    }

    if (this->coordinate.y <= this->boardSize - 2)
    {
        attackableCoordinates.push_back(Coordinate(this->coordinate.x, this->coordinate.y + 1));
    }

    return attackableCoordinates;
}

vector<Coordinate> Tank::getMoveableCoordinates()
{
    vector<Coordinate> moveableCoordinates;

    if (this->coordinate.x >= 1)
    {
        moveableCoordinates.push_back(Coordinate(this->coordinate.x - 1, this->coordinate.y));
    }

    if (this->coordinate.y >= 1)
    {
        moveableCoordinates.push_back(Coordinate(this->coordinate.x, this->coordinate.y - 1));        
    }

    if (this->coordinate.x <= this->boardSize - 2)
    {
        moveableCoordinates.push_back(Coordinate(this->coordinate.x + 1, this->coordinate.y));
    }

    if (this->coordinate.y <= this->boardSize - 2)
    {
        moveableCoordinates.push_back(Coordinate(this->coordinate.x, this->coordinate.y + 1));
    }

    return moveableCoordinates;
}

vector<Coordinate> Tank::getHealableCoordinates()
{
        return vector<Coordinate>();
}